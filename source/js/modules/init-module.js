/////////////////////
//// INIT MODULE ////
/////////////////////
var InitModule = (function(Modernizr) {
  'use strict';


  ////////////////////////////
  //// VARIABLES PRIVADAS ////
  ////////////////////////////
  var vars = {},
    methods = {},
    viewToLoad = location.search.substring(1),
    mobileDetection,
    browserDetection,
    footerExtraNewWidth;







  ////////////////////////////
  //// VARIABLES GLOBALES ////
  ////////////////////////////
  vars = {
    isMobile: false,
    firefox: false,
    ie9: false,
    ie10: false,
    ie11: false,
    $footerExtra: $('.footer-extra')
  };






  //////////////////////////
  //// METODOS PRIVADOS ////
  //////////////////////////
  mobileDetection = function() {
    if (window.Detectizr.device.type !== 'desktop' || Modernizr.mq('(max-width: 1200px)')) {
      vars.isMobile = true;
    } else {
      vars.isMobile = false;
    }
  };


  browserDetection = function() {
    if (window.Detectizr.browser.name === 'ie') {
      if (window.Detectizr.browser.major === '9') {
        vars.ie9 = true;
      }
      if (window.Detectizr.browser.major === '10') {
        vars.ie10 = true;
      }
      if (window.Detectizr.browser.major === '11') {
        vars.ie11 = true;
      }
      if (window.Detectizr.browser.major < 9) {
        $('.update-browser').show(0);
        $('.main').hide(0);
      }
    }
    if (window.Detectizr.browser.name === 'firefox') {
      $('.x_tab_celeste').css('top', '60px');
      $('.x_tab_amarillo').css('top', '60px');
    }
  };






  //////////////////////////
  //// METODOS PUBLICOS ////
  //////////////////////////

  methods.ready = function() {

    //DETECTAR SI ES MOBILE
    mobileDetection();

    //DETECTAR NAVEGADORES
    browserDetection();


    //CARGA DE VISTAS (SOLO EN FRONTEND)
    if (viewToLoad.length > 0) {
      $('.main-content').load(viewToLoad + '.html', function() {
        //INICIAR
        methods.init();
      });
    } else {
      //INICIAR
      methods.init();
    }
  };


  methods.init = function() {

    $(document).on( 'click','.menu-title', function(){
      $(this).toggleClass('menu-title-desplegable');
    });

    //EJECUTAR FUNCIONES DEL LOGIN
    LoginModule.methods.init();

    //BOTONES
    methods.eventsList();

    //TAMAÑO DEL ADORNO DEL FOOTER
    methods.calcFooterExtraSize();

    //CHECKBOX Y RADIO PERSONALIZADOS
    $('.icheck').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });


    //SI ES MOVIL
    if (vars.isMobile) {
      //IMPEDIR EFECTO ROLLOVER EN MÓVILES
      $('.hover').removeClass('hover');
    } else {
      //BARRA SCROLL PERSONALIZADA
      $('.scrollbox').enscroll({
        showOnHover: false,
        verticalTrackClass: 'track3',
        verticalHandleClass: 'handle3',
        pollChanges: true
      });
    }





    methods.resizeActions();
  };


  methods.eventsList = function() {
    //FUNCIONAMIENTO BTN DE AYUDA
    $(document).on("click", ".help-cloud .fa-times", methods.toggleHelpCloud);

    //SCROLL SUAVIZADO
    $(document).on("click", ".smoothscroll", methods.smoothScroll);

    //FUNCIONES QUE SE DEBEN EJECUTAR EN EL RESIZE
    $(window).on('resize', methods.resizeActions);

    //TABS EN INTERNAS
    $(document).on("click", ".menu-group-btn", methods.showTabContent);

    //ABRI MENÚ EN MOBILE
    $(document).on("click", ".menu-title", methods.showNav);

    //TABAS DE CONTENIDOS
    $(document).on('click','button', methods.showContentTabs);

    //MENU DESPEGABLE DESKTOP
    $(document).on('click','#meDesktopTrue', methods.changeMenuDesktop);
  };


  methods.resizeActions = function() {



    mobileDetection();

    browserDetection();

    methods.calcFooterExtraSize();

    //OCULTAR O MOSTRAR EL MENÚ SI ESTÁS EN MOBILE O DESKTOP
    if (vars.isMobile && Modernizr.mq('(max-width: 1010px)')) {
      $('.menu-groups').fadeOut(0);
      $('.menu-title').children('.fa').removeClass('fa-times').addClass('fa-bars');
    } else {
      $('.menu-groups').fadeIn(0);
      $('.menu-title').children('.fa').removeClass('fa-times').addClass('fa-bars');
    }



    if (Modernizr.mq('(max-width: 550px)')) {
      $('.inner-top-data .inner-top-text.second').prependTo('.main');
    } else {
      $('.inner-top-text.second').prependTo('.inner-top-data');
    }
  };


  methods.showNav = function() {
    $(this).children('.fa').toggleClass('fa-bars fa-times');
    $(this).next('.menu-groups').slideToggle();
  };

  methods.changeMenuDesktop = function(){
    $('#meDesktopFalse').removeClass('hidden');
    $('#meDesktopTrue').addClass('hidden');
  };

  methods.showContentTabs = function(){
      var atributo = $(this).attr('data-tabs');
      $('.content-info').addClass('hidden');
      $('#' + atributo).removeClass('hidden');
  };

  methods.showTabContent = function() {
    $(this).parent().siblings('.menu-group').find('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');
    $(this).parent().siblings('.menu-group').removeClass('active');
    $(this).parent().toggleClass('active');


    $(this).find('.x_tab_celeste').removeClass('hidden');
    $(this).parent().siblings('.menu-group').find('.x_tab_celeste').addClass('hidden');

    $(this).find('.x_tab_amarillo').removeClass('hidden');
    $(this).parent().siblings('.menu-group').find('.x_tab_amarillo').addClass('hidden');

    $(this).children('.fa').toggleClass('fa-chevron-up fa-chevron-down');
  };


  methods.calcFooterExtraSize = function() {
    footerExtraNewWidth = ($(window).width() - 960) / 2;
    vars.$footerExtra.css('width', footerExtraNewWidth + 'px').delay(300).fadeIn(0);
  };


  methods.toggleHelpCloud = function() {
    $(this).parent().toggleClass('active');
    $('.help-cloud-content').stop().slideToggle(300);
  };


  methods.smoothScroll = function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

      var $target = $(this.hash);
      $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');

      if ($target.length) {
        var targetOffset = $target.offset().top;
        $('html,body').animate({
          scrollTop: targetOffset
        }, 700);
        return false;
      }
    }
  };






  return {
    methods: methods,
    vars: vars
  };


})(Modernizr);





//CUANDO HA CARGADO EL DOM
$(document).ready(InitModule.methods.ready);
