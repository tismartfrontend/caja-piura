//////////////////////
//// LOGIN MODULE ////
//////////////////////
var LoginModule = (function(Modernizr) {
  'use strict';


  ////////////////////////////
  //// VARIABLES PRIVADAS ////
  ////////////////////////////
  var vars = {},
    methods = {},
    place,
    keyNumber,
    passwordKeys = '',
    randomKeyboard,
    printKeyValue,
    view_detailsNumber_mobil,
    detailsNumber_mobil,
    click_backDetails,
    back_Details,
    keyboardEvents,
    resetCountDown,
    hideKeyboardOnMobile,
    countDown,
    passwordPointer = '#login-user-password',
    setPasswordPointer,
    targetType,
    shuffleArray,
    cleanPassword,
    numberGroup = [],
    numberHtml = '',
    countDownNumber = 45,
    keyboardNumbers = ['1', '5', '2', '6', '4', '8', '3', '7', '9', '0'];





  ////////////////////////////
  //// VARIABLES GLOBALES ////
  ////////////////////////////
  vars = {};






  //////////////////////////
  //// METODOS PRIVADOS ////
  //////////////////////////
  shuffleArray = function(array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  };


  randomKeyboard = function(array) {
    //DESORDENAR ARRAY
    shuffleArray(array);

    //AGRUPAR NÚMEROS
    array.forEach(function(el, i) {
      numberHtml = '<button class="btn btn-blue hover keyboard-key" data-number="' + el + '">' + el + '</button>';
      numberGroup.push(numberHtml);
    });
    numberGroup = numberGroup.join('');

    //AGREGAR LAS TECLAS
    $('.password-keyboard').prepend(numberGroup);
    numberGroup = [];
  };


  countDown = function() {
    countDownNumber--;
    $('.countdown-number').html(countDownNumber);

    if (countDownNumber !== 0) {
      setTimeout(function() {
        countDown(countDownNumber);
      }, 1000);
    } else {
      console.log('Cerrar');
    }
  };


  keyboardEvents = function() {
    //AGREGAR DIGITO DE LA TECLA
    $(document).on("click", ".keyboard-key", printKeyValue);

    //BORRAR LO DIGITADO POR LAS TECLAS
    $(document).on("click", ".clean-password", cleanPassword);

    //REINICIAR CONTEO PARA CERRAR VENTANA AL USAR EL MOUSE O TECLADO
    $(document).on("mousemove", "html", resetCountDown);
    $(document).on("keyup", "html", resetCountDown);


    //SETEAR INPUT ELEGIDO PARA ESCRIBIR CONTRASEÑA EN EL
    $(document).on("click", ".panel-step input:not(.not-keyboard-here)", setPasswordPointer);

    //OCULTAR TECLADO EN MÓVILES
    $(document).on("click", ".btn-keyboard-ok", hideKeyboardOnMobile);

  };

  view_detailsNumber_mobil = function(){
    $(document).on('click','.click-showDetails', detailsNumber_mobil);
  };

  click_backDetails = function(){
    $(document).on('click','#click-backDetails', back_Details);
  };

  setPasswordPointer = function (event) {
    passwordKeys = '';
    targetType = $(event.target).attr('type');

    if (targetType === 'password' || targetType === 'hidden' ) {
      passwordPointer = event.target;
    }

    if (InitModule.vars.isMobile) {
      $('#keyboard-container').addClass('active');
    }
  };


  hideKeyboardOnMobile = function () {
    $('#keyboard-container').removeClass('active');
  };

  detailsNumber_mobil = function(){
    $('.showDetailsNumber_mobil').removeClass('hidden');
    $('.mb').removeClass('visible-xs');
    $('.mb').addClass('hidden');
    $('.showDetailsNumber_mobil').addClass('showDetailsNumber_mobil_block');
    console.log('Prueba de vista');
  };

  back_Details = function(){
    $('.showDetailsNumber_mobil').removeClass('showDetailsNumber_mobil_block');
    $('.mb').removeClass('hidden');
    $('.showDetailsNumber_mobil').addClass('hidden');
    $('.mb').addClass('hidden-sm  hidden-md');
    console.log('exito back');
  };

  printKeyValue = function(event) {
    event.preventDefault();

    // if (passwordKeys.length < 6) {
      keyNumber = $(this).attr('data-number');
      passwordKeys += keyNumber;

      $(passwordPointer).val(passwordKeys);
    // }
  };


  cleanPassword = function(event) {
    event.preventDefault();
    passwordKeys = '';
    $(passwordPointer).val('');
  };


  resetCountDown = function() {
    countDownNumber = 45;
  };



  //////////////////////////
  //// METODOS PUBLICOS ////
  //////////////////////////
  methods.init = function() {
    //IMPEDIR INGRESO DE LETRAS EN INPUTS NUMÉRICOS
    $('.input-number').numeric();

    $('.datepicker').datepicker();

    view_detailsNumber_mobil();

    click_backDetails();

    //TECLADO ALEATORIO
    randomKeyboard(keyboardNumbers);


    //CONTADOR ANTES DE CERRAR LA VENTANA
    countDown();


    //FUNCIONAMIENTO DEL TECLADO
    keyboardEvents();

  };





  return {
    methods: methods,
    vars: vars
  };


})(Modernizr);
